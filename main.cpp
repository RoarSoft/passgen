/*
 *	Copyright (C) 2017 Andrew Barnes, Richard Bond [rich.bond@roarsoft.co.uk]
 *	This file is part of PassGen.
 *
 *	PassGen is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	PassGen is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	(COPYING in the root folder) along with PassGen.
 *	If not, see <http://www.gnu.org/licenses/>.
 */

#include "passgen.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CPassGen w;
    w.show();

    return a.exec();
}
