 #
 #	Copyright (C) 2017 Andrew Barnes, Richard Bond [rich.bond@roarsoft.co.uk]
 #	This file is part of PassGen.
 #
 #	PassGen is free software: you can redistribute it and/or modify
 #	it under the terms of the GNU General Public License as published by
 #	the Free Software Foundation, either version 3 of the License, or
 #	(at your option) any later version.
 #
 #	PassGen is distributed in the hope that it will be useful,
 #	but WITHOUT ANY WARRANTY; without even the implied warranty of
 #	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #	GNU General Public License for more details.
 #
 #	You should have received a copy of the GNU General Public License
 #	(COPYING in the root folder) along with PassGen.
 #	If not, see <http://www.gnu.org/licenses/>.
 #

#-------------------------------------------------
#
# Project created by QtCreator 2017-01-01T14:05:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PassGen
TEMPLATE = app

LIBS+=-lssl -lcrypto

SOURCES += main.cpp\
        passgen.cpp

HEADERS  += passgen.h

FORMS    += cpassgen.ui
