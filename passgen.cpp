/*
 *	Copyright (C) 2017 Andrew Barnes, Richard Bond [rich.bond@roarsoft.co.uk],
 *	This file is part of PassGen.
 *
 *	PassGen is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	PassGen is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	(COPYING in the root folder) along with PassGen.
 *	If not, see <http://www.gnu.org/licenses/>.
 */

#include "passgen.h"
#include "ui_cpassgen.h"
#include <openssl/sha.h>

#include <QMessageBox>
#include <QTextStream>
#include <QDebug>
#include <QUrl>
#include <cmath>
using namespace std;

CPassGen::CPassGen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CPassGen)
{
    ui->setupUi(this);
}

CPassGen::~CPassGen()
{
    delete ui;
}

void CPassGen::on_pbGenerate_clicked()
{

    QByteArray outBuffer;
    outBuffer.fill(2,64);

    QList<QString> inputData;
    inputData.push_back(ui->lePassword->text());
    inputData.push_back(ui->leWebAddress->text());
    inputData.push_back(ui->leWebUsername->text());

    QString inBuff = jumble(inputData);
    qDebug() << "Jumbled: " << inBuff;
    SHA512((const unsigned char*)inBuff.toLatin1().data(),inBuff.size(),(unsigned char*)outBuffer.data());

    QString passText;
    QTextStream passStream(&passText);
    for(int i = 0; i < outBuffer.size(); i++)
    {
        unsigned char cVal = outBuffer.at(i);
        char  passChar = 33+(cVal%93);
        passStream << passChar;
    }

    ui->lPassword1->setText(shaToPassword(passText,0,20));
    ui->lPassword2->setText(shaToPassword(passText,1,20));
    ui->lPassword3->setText(shaToPassword(passText,2,20));
    ui->lPassword4->setText(shaToPassword(passText,3,20));
    ui->lPassword5->setText(shaToPassword(passText,4,20));
}

QString CPassGen::shaToPassword(QString sha, int offset, int length)
{
    QString password;
    password.fill(0,length);

    int jumpSize=(7+offset+(offset%2))%sha.length();

    int pos = offset%sha.length();
    bool hasUppercase = false;
    bool hasLowercase = false;

    for(int i = 0; i < length; i++)
    {
        char passChar = sha[pos].toLatin1();
        if(sha[pos].isLetter())
            passChar = ((i%2)?::tolower(passChar): ::toupper(passChar));

        password[i] = passChar;

        if(password[i].isUpper())
            hasUppercase = true;
        else if (password[i].isLower())
            hasLowercase = true;

        pos = (pos+jumpSize)%sha.length();
    }

    int letterIndex = 0;
    if(!hasUppercase)
    {
        for(; letterIndex < password.length(); letterIndex++)
        {
            if(password[letterIndex].isLetter())
                break;
        }

        if(letterIndex < password.length())
            password[letterIndex] = ::toupper(password[letterIndex].toLatin1());
        else
            password[length/2] = 65+(password[length/2].toLatin1()%25);

        letterIndex++;
    }

    if(!hasLowercase)
    {
        for(; letterIndex < password.length(); letterIndex++)
        {
            if(password[letterIndex].isLetter())
                break;
        }

        if(letterIndex < password.length())
            password[letterIndex] = ::tolower(password[letterIndex].toLatin1());
        else
        {
            password[(length/2)+1] = 97+(password[(length/2)+1].toLatin1()%25);
        }
    }

    return password;
}

QString CPassGen::urlToHost(QUrl url)
{
    return url.host().isEmpty()?url.url(QUrl::StripTrailingSlash):url.host();
}

void CPassGen::on_leWebAddress_editingFinished()
{
    ui->leWebAddress->setText(urlToHost(ui->leWebAddress->text()));
}

QString CPassGen::jumble(QList<QString> originals)
{
    if(originals.isEmpty())
        return QString();

    int maxInputLength = 0;

    for(QList<QString>::iterator origIt = originals.begin(); origIt != originals.end(); origIt++)
        maxInputLength = max(maxInputLength,origIt->length());

    QString jumbled;
    QTextStream jumbler(&jumbled);

    for(int i = 0; i < maxInputLength; i++)
    {
        for(QList<QString>::iterator origIt = originals.begin(); origIt != originals.end(); origIt++)
            jumbler << origIt->at(i%origIt->length());
    }
    return jumbled;
}

void CPassGen::on_pbAbout_clicked()
{
    QMessageBox::about(this,"About PassGen","Passgen is a secure password generation tool.\n"
                                            "Passgen is based on a repeatable password generation\n"
                                            "concept by Andrew Barnes\n\n"
                                            "PassGen is free software released under the GPLv3 licence\n"
                                            "The PassGen source code can be found here:\n"
                                            "https://bitbucket.org/RoarSoft/passgen");
}
