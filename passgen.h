/*
 *	Copyright (C) 2017 Andrew Barnes, Richard Bond [rich.bond@roarsoft.co.uk]
 *	This file is part of PassGen.
 *
 *	PassGen is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	PassGen is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	(COPYING in the root folder) along with PassGen.
 *	If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PASSGEN_H
#define PASSGEN_H

#include <QDialog>
#include <QUrl>

namespace Ui {
class CPassGen;
}

class CPassGen : public QDialog
{
    Q_OBJECT

public:
    explicit CPassGen(QWidget *parent = 0);
    ~CPassGen();

private slots:
    void on_pbGenerate_clicked();
    void on_leWebAddress_editingFinished();
    void on_pbAbout_clicked();

private:
    QString jumble(QList<QString> originals);
    QString shaToPassword(QString sha, int offset, int length);
    QString urlToHost(QUrl url);

    Ui::CPassGen *ui;
};

#endif // PASSGEN_H
